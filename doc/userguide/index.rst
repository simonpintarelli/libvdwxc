libvdwxc -- A library for vdW-DF exchange-correlation functionals
=================================================================

`Download most recent release <https://launchpad.net/libvdwxc/stable/0.3.2/+download/libvdwxc-0.3.2.tar.gz>`_

:program:`libvdwxc` is a general library for evaluating energy and
potential for exchange-correlation (XC) functionals from the vdW-DF
family that can be used with various of density functional theory
(DFT) codes. This work was inspired by success of :program:`libXC`, a
library for local and semilocal XC functionals. At the moment,
:program:`libvdwxc` provides access to the DF1, DF2, and CX
functionals and interfaces for `GPAW
<https://wiki.fysik.dtu.dk/gpaw/>`_ and `Octopus
<http://www.tddft.org/programs/octopus/wiki/index.php/Main_Page>`_. The
library has been tested with respect to the S22 test set, various bulk
properties of metals and semiconductors, and surface energies.
 
:program:`libvdwxc` and its development are hosted on
`gitlab <https://gitlab.com/libvdwxc/libvdwxc>`_. Bugs and feature
requests are ideally submitted via the `gitlab issue
tracker <https://gitlab.com/libvdwxc/libvdwxc/issues>`_. The
development team can also be reached by email via
questions@libvdwxc.org.

If you use :program:`libvdwxc` in your research please include
the following citation in publications or presentations:

* A. H. Larsen, M. Kuisma, J. Löfgren, Y. Pouillon, P. Erhart, and P. Hyldgaard,
  *libvdwxc: a library for exchange–correlation functionals in the vdW-DF family*,
  Modelling Simul. Mater. Sci. Eng. **25**, 065004 (2017),
  `doi: 10.1088/1361-651X/aa7320
  <http://dx.doi.org/10.1088/1361-651X/aa7320>`_

Contents
-----------

.. toctree::
   :maxdepth: 2

   introduction
   background
   benchmarks
   configuring-libvdwxc
   contributing-to-libvdwxc
   hacking-the-build-system
   authors
   bibliography

Indices and tables
-----------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

