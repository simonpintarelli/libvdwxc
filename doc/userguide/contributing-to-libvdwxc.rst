.. _contributing:
.. index:: Contributing

Contributing to :program:`libvdwxc`
**********************************************

:program:`libvdwxc` follows the GNU Coding Standards as much as
permitted by the experience of its developers. If you are planning to
contribute, we thus recommend you to consult the official `GNU Coding
Standards homepage <https://www.gnu.org/prep/standards/>`_ before
doing so.


.. index:: Contributing; Programming language

Programming language
====================================

The core of :program:`libvdwxc` is written in C. All other languages
must be supported through bindings.

Since we use the Autotools to build, install, and distribute the code,
all C, C++, or Fortran files, must be stored in language-specific
subdirectories.  Mixes should not happen in the same directory, and
even less in the same library.


.. index:: Contributing; Optional features

Optional features
====================================

Optional features are triggered either through `HAVE_FEATURE` or
`VDW_FEATURE` preprocessing options, where `FEATURE` is the name of
the feature. All these preprocessing options are handled through
`config.h`, with the exception of the Autotools-reserved
`HAVE_CONFIG_H` option.

Within the source code, access to the optional features is granted in
a boolean way, i.e.::

    #if defined HAVE_FEATURE
    ... do something optional ...
    #endif

This allows keeping the build system as simple and maintainable as
possible.


.. index:: Contributing; C header files

C header files
====================================

In order to keep the build process smooth and simple, we have adopted
the following conventions for the inclusion of C headers:

* system headers should come first, then external dependency headers,
  then internal exported headers, then internal non-exported headers,
  then `config.h`, and finally non-exported headers depending on
  `config.h` and/or conditional inclusions;
* all headers coming from outside :program:`libvdwxc` must be included
  using "less than" and "greater than" as delimiters;
* internal headers must be included using double quotes as delimiters;
* the inclusion of `config.h` must be controlled by the
  `HAVE_CONFIG_H` preprocessing option;
* exported headers must not depend on `config.h` nor on non-exported
  headers;
* all exported header names (apart from `vdwxc.h`) must be prefixed
  with `vdwxc_`;
* headers depending on `config.h` should be minimised;
* `config.h` must be readable from Fortran.

Here is an example where the source code depends on the FFT library
available::

    #include <stdlib.h>
    #include <stdio.h>
    #include <assert.h>
    #include <string.h>

    #include "vdwxc.h"
    #include "vdw_splines.h"

    #if defined HAVE_CONFIG_H
    #include "config.h"
    #endif

    #ifdef HAVE_PFFT
    #include <pfft.h>
    #else
    #ifdef HAVE_MPI
    #include <fftw3-mpi.h>
    #else
    #include <fftw3.h>
    #endif
    #endif


Keeping the build system updated
====================================

When changing the contents of a file, the build system does not need
to be updated. However, when adding, removing, or renaming files, you
have to inform the build system of what happened. The corresponding
procedures are described in [Hacking the build
system](hacking-the-build-system.html).


.. index:: Contributing; Testing

Testing
====================================

When adding new functions, it is extremely important to provide simple
test programs, aka "unit tests", to check whether these functions are
performing as they should. If you do not feel comfortable with
integrating these tests with the build system, please notify the other
developers.

Systematically writing unit tests is not only essential to maintain
the overall quality of the code. It will greatly help efficiently
design and structure your contributions, since you will always have a
concrete use example at hand to feed your thoughts.

