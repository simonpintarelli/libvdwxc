#!/usr/bin/env python3
from __future__ import print_function

description = """
Make a new libvdwxc release
---------------------------

 * Be in a clean-ish source directory.
 * Run autoreconf -i and ./configure with reasonably generic parameters
 * Run this file.
 * If anything goes wrong, figure out what this file does and fix it.

This script basically does the following:

 * <update version number in configure.ac>
 * git checkout -b <version>
 * make dist
 * gpg --armor --yes --detach-sign libvdwxc-<version>.tar.gz
 * git add configure.ac
 * git commit -m "libvdwxc <version>"
 * git tag <version>
 * <bump version number in configure.ac>
 * git add configure.ac
 * git commit -m "bump version number to <version>"
 * <edit link to tarball on webpage>
 * git add doc/homepage/index.html
 * git commit -m "Update link to tarball for libvdwxc-<version>"

This creates a tarball named libvdwxc-<version>.tar.gz.
That tarball is the release.  The tagged version is not quite the release.

This script should be run from the master branch, and HEAD will be used
to generate the new release.

On version numbering:

The minor version number is uneven for development and even for releases.

Example:

Current development version: 0.2.7
Release: either 0.2.8, 0.3.0, or 1.0.0
Development version becomes 0.2.9, 0.3.1, or 1.0.1

After running the script, revise that things are okay and merge into master.
Then upload tarball and detached signature (.asc) to launchpad.
Then push to origin master.
"""

import re
from subprocess import check_call, check_output
from argparse import ArgumentParser, RawDescriptionHelpFormatter


version_pat = re.compile(r'AC_INIT\(\[libvdwxc\],'
                         r'\s*\[(.+?)\],'
                         r'\s*\[asklarsen@gmail.com\]\)')


def read_configure_version():
    with open('configure.ac') as fd:
        for line in fd:
            m = version_pat.match(line)
            if m:
                return m.group(1)
        else:
            raise IOError('Version not found')

def edit_configure_ac(version):
    lines = []
    with open('configure.ac') as fd:
        lineno = None
        for i, line in enumerate(fd):
            m = version_pat.match(line)
            if m is not None:
                line = ('AC_INIT([libvdwxc], [{}], [asklarsen@gmail.com])\n'
                        .format(version))
                assert lineno is None
                lineno = i
            lines.append(line)

    if lineno is None:
        raise IOError('Cannot find version')

    with open('configure.ac', 'w') as fd:
        for line in lines:
            fd.write(line)

webpagefname = 'doc/homepage/index.html'


def edit_homepage(version):
    # Update link to tarball on launchpad
    lines = []
    found = False
    with open(webpagefname) as fd:
        for line in fd:
            if line.lstrip().startswith('href="https://launchpad.net/'
                                        'libvdwxc/stable/'):
                line = ('href="https://launchpad.net/libvdwxc/stable/'
                        '{v}/+download/libvdwxc-{v}.tar.gz"\n'
                        .format(v=version))
                assert not found
                found = True
            lines.append(line)
    assert found
    with open(webpagefname, 'w') as fd:
        for line in lines:
            fd.write(line)



currentversion = read_configure_version()

parser = ArgumentParser(description=description,
                        formatter_class=RawDescriptionHelpFormatter)
parser.add_argument('VERSION')
parser.add_argument('--version', action='version',
                    version='libvdwxc {}'.format(currentversion))
args = parser.parse_args()
version = args.VERSION


def ccall(args, **kwargs):
    if hasattr(args, 'swapcase'):
        args = args.split()
    return check_call(args, **kwargs)


def coutput(args, **kwargs):
    if hasattr(args, 'swapcase'):
        args = args.split()
    return check_output(args, **kwargs).decode('utf8')


def getbranch():
    txt = coutput('git status')
    m = re.match(r'On branch (.+)', txt)
    branch = m.group(1)
    return branch


print('New version: {}'.format(version))

branch = getbranch()
if branch != version:
    ccall('git checkout -b release-{}'.format(version))
    assert getbranch() == 'release-{}'.format(version)

edit_configure_ac(version=version)
numbers = version.split('.')
nextminor = int(numbers[-1]) + 1
new_dev_version = '.'.join(numbers[:-1] + [str(nextminor)])

ccall('make dist')
tarball = 'libvdwxc-{}.tar.gz'.format(version)
ccall('gpg --armor --yes --detach-sign {}'.format(tarball))
ccall('git add configure.ac')
ccall(['git', 'commit', '-m', 'libvdwxc {}'.format(version)])
ccall(['git', 'tag', '-s', version, '-m "libvdwxc-{}"'.format(version)])
edit_configure_ac(version=new_dev_version)
ccall('git add configure.ac')
ccall(['git', 'commit', '-m',
       'bump version number to {}'.format(new_dev_version)])
edit_homepage(version)
ccall(['git', 'add', webpagefname])
ccall(['git', 'commit', '-m',
       'Update link to tarball for libvdwxc-{}'.format(version)])
ccall('git status')

print()
print('Done creating new source distribution libvdwxc {}.'.format(version))
print()
print('Remaining steps:\n'
      '  Check new version number in configure.ac\n'
      '  Merge back into master\n'
      '  Publish the tar file {tarball}\n'
      '  Create new milestone and release on launchpad\n'
      '  Upload tarball and signature to launchpad\n'
      '  Push master to origin master including --tags\n'
      '\n'
      'Or to undo, run:\n'
      '  git checkout master\n'
      '  git tag -d {version}\n'
      '  git branch -D release-{version}\n'
      '  rm {tarball}\n'.format(version=version, tarball=tarball))
