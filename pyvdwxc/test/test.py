import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
import numpy as np
from vdwxc import find_vdw_library
from vdwxc import VDWXC

def check_for_library():
    lib = find_vdw_library()
    assert hasattr(lib, 'vdw_df_data_get_size'), ('Library does not expose '
                                                  'expected interface')

def check_vdw_class():
    lib = VDWXC()

def calculate():
    from gpaw.mpi import world as comm

    ngpts = 32
    a = 6.0
    assert ngpts % comm.size == 0, 'npoints not divisible by comm size'
    global_shape = (ngpts, ngpts, ngpts)
    local_shape = (ngpts // comm.size, ngpts, ngpts)
    cell = (a, a, a)
    center = np.array(cell) / 2.0
#    vdw.set_unit_cell(shape, cell)
#    g_i = np.arange(np.product(shape))
#    vdw.set_topology(g_i)
#c
#    r = np.linspace(0.0, a, ngpts)
#    r_vg = np.array(np.meshgrid(r, r))#, r

    from gpaw.grid_descriptor import GridDescriptor
    gd = GridDescriptor(N_c=global_shape,
                        cell_cv=cell,
                        parsize=(comm.size, 1, 1))
    r_vg = gd.get_grid_point_coordinates()
    dist2_g = ((r_vg - center[:, None, None, None])**2).sum(axis=0)

    vdw = VDWXC()
    vdw.set_unit_cell(global_shape, local_shape, cell)
    g_i = np.arange(np.product(local_shape))
    # len(g_i) must be Nlocx * Ny * Nz.
    vdw.set_topology(g_i)

    #r = np.linspace(0.0, a, ngpts)
    #r_vg = np.array(np.meshgrid(r, r, r))

    n_g = np.exp(-dist2_g)
    sigma_g = n_g**2

    print 'local shape', local_shape

    def plot_slices(arr):
        amax = arr.max()
        amin = arr.min()
        import pylab as pl
        for arr1 in arr:
            arr1 = arr1.copy()
            arr1[0, 0] = amax
            arr1[0, 1] = amin
            pl.matshow(arr1)
        pl.show()

    #print n_g[16, 16, :]
    #print sigma_g[16, 16, :]

    #n_g = np.ones(shape, float)
    #sigma_g = np.ones(shape, float)
    energy, dedrho_g, dedsigma_g = vdw.calculate(n_g, sigma_g)

    #plot_slices(dedrho_g)
    #plot_slices(dedsigma_g)

    print 'energy', gd.comm.sum(energy)

def main():
    #check_for_library()
    #check_vdw_class()
    calculate()

if __name__ == '__main__':
    main()
