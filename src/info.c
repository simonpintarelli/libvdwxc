#include <stdio.h>
#include "vdwxc.h"

int main(int argc, char **argv)
{
    printf("libvdwxc\n");
    printf("    Compiled with MPI:  %3s\n", (vdwxc_has_mpi() ? "yes" : "no"));
    printf("    Compiled with PFFT: %3s\n", vdwxc_has_pfft() ? "yes" : "no");
}
