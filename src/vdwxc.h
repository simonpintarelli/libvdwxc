#ifndef VDWXC_H
#define VDWXC_H

// libvdwxc can be compiled in three ways:
//  * libvdwxc + fftw
//  * libvdwxc + fftw + mpi + fftw-mpi
//  * libvdwxc + fftw + mpi + fftw-mpi + pfft

#ifdef __cplusplus
extern "C" {
#endif

#include <complex.h>


#define VDWXC_DF1 1
#define VDWXC_DF2 2
#define VDWXC_FUNCTIONAL_CUSTOM -1

#define VDWXC_FFTW_SERIAL 0
#define VDWXC_FFTW_MPI 1
#define VDWXC_PFFT 2

// FUNC_xxxx and VDW_xxxx deprecated
// in favour of VDWXC_xxxx
#define FUNC_VDWDF VDWXC_DF1
#define FUNC_VDWDF2 VDWXC_DF2
#define FUNC_VDWDFCX 3
#define FUNC_CUSTOM VDWXC_FUNCTIONAL_CUSTOM
#define VDW_FFTW_SERIAL VDWXC_FFTW_SERIAL
#define VDW_FFTW_MPI VDWXC_FFTW_MPI
#define VDW_PFFT VDWXC_PFFT

#define VDWXC_HAS_SPIN 1

typedef struct vdwxc_data_obj *vdwxc_data;

// This header file declares the high level functions that are called
// by DFT codes.

vdwxc_data vdwxc_new(int functional);
vdwxc_data vdwxc_new_spin(int functional);

void vdwxc_print(vdwxc_data data);
int vdwxc_tostring(vdwxc_data data, int maxsize, char *str);

void vdwxc_set_unit_cell(vdwxc_data data,
                         int Nx, int Ny, int Nz,
                         double C00, double C10, double C20,
                         double C01, double C11, double C21,
                         double C02, double C12, double C22);

// Set the radial grid for vdW-DF calculations.
//void vdwxc_df_set_radial_grid(vdwxc_data data, double h, int Nr);

// These are functions because it seems more portable than constants
// For example they can be called through Python ctypes and such.
int vdwxc_has_mpi(void);
int vdwxc_has_pfft(void);

void vdwxc_init_serial(vdwxc_data data);

double vdwxc_calculate(vdwxc_data data,
                       double *rho_g, double *sigma_g,
                       double *dedn_g, double *dedsigma_g);

double vdwxc_calculate_spin(vdwxc_data data,
                            double *rho_up_g, double *rho_dn_g,
                            double *sigma_up_g, double *sigma_dn_g,
                            double *dedn_up_g, double *dedn_dn_g,
                            double *dedsigma_up_g, double *dedsigma_dn_g);

//double vdwxc_calculate_radial(vdwxc_data data, int N, double dr, double* rho_i,
//                            double* sigma_i, double* dedn_i, double* dedsigma_i);


void vdwxc_finalize(vdwxc_data* data);

void vdwxc_get_q0(vdwxc_data data, double *q0);

// Functions for more or less internal use.
//void vdwxc_allocate_buffers(vdwxc_data data);


/*
Input:
  int N : number of grid points
  double Z_ab
  double q_cut
  double* rho : density (in electrons / Bohr^3)
  double* sigma : |\nabla \rho|^2

Output:
  double* q0 : q0
  double* dq0_drho : Derivative of q0 wrt. rho
  double* dq0_dsigma : Derivative of q0 wrt. gradient of rho
*/
#ifdef __cplusplus
}
#endif

#endif
