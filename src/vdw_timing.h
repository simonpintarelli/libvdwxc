#ifndef VDW_TIMING_H
#define VDW_TIMING_H

#include <time.h>

static int timer_level=0;
static int timer_count=0;

#define START_TIMER(str, message) \
   printf("TIMER: ");                           \
   for (timer_count=0; timer_count<timer_level; timer_count++) \
     printf("   "); \
   printf("Started ");            \
   printf(message);                \
   printf(".\n");                \
   timer_level++; \
   clock_t time_begin##str, time_end##str; \
   double time_spent##str; \
   time_begin##str = clock();

#define END_TIMER(str, message) \
   printf("TIMER: "); \
   timer_level--; \
   time_end##str = clock(); \
   time_spent##str = (double)(time_end##str - time_begin##str) / CLOCKS_PER_SEC; \
   for (timer_count=0; timer_count<timer_level; timer_count++) \
     printf("   "); \
   printf(message); \
   printf(" took %.3f seconds.\n", time_spent##str);

#endif
