  interface
    subroutine vdwxc_new(functional, vdw)
      integer, intent(in) :: functional
      integer, pointer, intent(out) :: vdw
    end subroutine vdwxc_new
  end interface

  interface
    subroutine vdwxc_print(vdw)
      integer, pointer, intent(in) :: vdw
    end subroutine vdwxc_print
  end interface

  interface
    subroutine vdwxc_tostring(vdw, maxlen, str, strlen)
      integer, pointer, intent(in) :: vdw
      integer, intent(in) :: maxlen
      character(1), intent(inout) :: str(:)
      integer, intent(out) :: strlen
    end subroutine vdwxc_tostring
  end interface

  interface
    subroutine vdwxc_set_unit_cell(vdw, nx, ny, nz,&
      C00, C01, C02, C10, C11, C12, C20, C21, C22)
      integer, pointer, intent(inout) :: vdw
      integer, intent(in) :: nx, ny, nz
      real(8), intent(in) :: C00, C01, C02, C10, C11, C12, C20, C21, C22
    end subroutine vdwxc_set_unit_cell
  end interface

  interface
    subroutine vdwxc_has_mpi(yesno)
      integer, intent(out) :: yesno
    end subroutine vdwxc_has_mpi
  end interface

  interface
    subroutine vdwxc_has_pfft(yesno)
      integer, intent(out) :: yesno
    end subroutine vdwxc_has_pfft
  end interface

  interface
    subroutine vdwxc_init_serial(vdw)
      integer, pointer, intent(inout) :: vdw
    end subroutine vdwxc_init_serial
  end interface

  interface
    subroutine vdwxc_init_mpi(vdw, mpi_comm)
      integer, pointer, intent(inout) :: vdw
      integer,          intent(in)    :: mpi_comm
    end subroutine vdwxc_init_mpi
  end interface

  interface
    subroutine vdwxc_init_pfft(vdw, mpi_comm, nx, ny)
      integer, pointer, intent(inout) :: vdw
      integer,          intent(in)    :: mpi_comm
      integer,          intent(in)    :: nx, ny
    end subroutine vdwxc_init_pfft
  end interface

  interface
    subroutine vdwxc_calculate(vdw, rho_g, sigma_g, dedrho_g, dedsigma_g, energy)
      integer, pointer, intent(inout) :: vdw
      ! rho and sigma should probably be intent(in)
      real(8), intent(inout) :: rho_g(:,:,:), sigma_g(:,:,:)
      real(8), intent(inout) :: dedrho_g(:,:,:), dedsigma_g(:,:,:)
      real(8), intent(inout) :: energy
    end subroutine vdwxc_calculate
  end interface

  interface
    subroutine vdwxc_finalize(vdw)
      integer, pointer, intent(inout) :: vdw
    end subroutine vdwxc_finalize
  end interface
