fid = fopen('vdw_splines.inc','w');
Nq = 20;
qmesh = [  1.00000000000000E-05   4.49420825586261E-02   9.75593700991365E-02   1.59162633466142E-01 ...
   2.31286496836006E-01   3.15727667369529E-01   4.14589693721418E-01   5.30335368404141E-01 ...
   6.65848079422965E-01   8.24503639537924E-01   1.01025438252095E+00   1.22772762136457E+00 ...
   1.48234092117491E+00   1.78043705835953E+00   2.12944202813364E+00   2.53805003653458E+00 ...
   3.01644008535668E+00   3.57652954544246E+00   4.23227103519872E+00   5.00000000000000E+00 ];
%[ 0.0, 0.01, 0.02, 0.1, 0.3, 1.0, 2.0, 3.0, 5.0  ];

for sindex=1:Nq
% Setup the spline values
vals = zeros(Nq,1);
vals(sindex) = 1.0;

% Here is linear spline interpolation
if 0
M = zeros(2*Nq-2, 2*Nq-2);
rhs = zeros(2*Nq-2,1);

% N-1 values
for i=1:Nq-1
  M( i, (i-1)*2+1) = 1.0;
  rhs(i) = vals(i);
end

% Final value
M( Nq, (Nq-2)*2+1 ) = 1.0;
M( Nq, (Nq-2)*2+2 ) = qmesh(Nq)-qmesh(Nq-1);
rhs( Nq ) = vals(Nq);

% N-2 continuity equations
for i=1:Nq-2
 M( Nq+i, (i-1)*2+1 ) = 1.0;
 M( Nq+i, (i-1)*2+2 ) = qmesh(i+1)-qmesh(i);
 M( Nq+i, i*2+1) = -1.0;
 rhs(Nq+i) = 0.0;
end

b = M \ rhs;
M
b
x = linspace(0.01,0.99,3000);
xx=[];
yy=[];
for i=1:1000
x0 = x(i); % Interpolate x0

i=find(x0 < qmesh);
lower_boundary = qmesh(i(1)-1);
y0 = b( (i(1)-2)*2 + 1 ); % Constant
y0 = y0 + b( (i(1)-2)*2 + 2 ) * (x0 - lower_boundary);
xx = [xx x0];
yy = [yy y0];
end
plot(xx,yy,'k');
%ylim([-0.1 1.1]);
hold on
end

% Here are cubic splines
if 1
M = zeros(4*Nq-4, 4*Nq-4);
rhs = zeros(4*Nq-4, 1);

% N-1 values
for i=1:Nq-1
  M( i, (i-1)*4+1) = 1.0;
  rhs(i) = vals(i);
end

% Final value
M( Nq, (Nq-2)*4+1 ) = 1.0;
dq = qmesh(Nq)-qmesh(Nq-1);
M( Nq, (Nq-2)*4+2 ) = dq;
M( Nq, (Nq-2)*4+3 ) = dq*dq;
M( Nq, (Nq-2)*4+4 ) = dq*dq*dq;
rhs( Nq ) = vals(Nq);

% N-2 continuity equations
for i=1:Nq-2
 M( Nq+i, (i-1)*4+1 ) = 1.0;
 dq = qmesh(i+1)-qmesh(i);
 M( Nq+i, (i-1)*4+2 ) = dq;
 M( Nq+i, (i-1)*4+3 ) = dq*dq;
 M( Nq+i, (i-1)*4+4 ) = dq*dq*dq;
 M( Nq+i, i*4+1) = -1.0;
 rhs(Nq+i) = 0.0;
end

% N-2 derivative continuity equations
for i=1:Nq-2
 M( 2*Nq-2+i, (i-1)*4+2 ) = 1.0;
 dq = qmesh(i+1)-qmesh(i);
 M( 2*Nq-2+i, (i-1)*4+2 ) = 1.0;
 M( 2*Nq-2+i, (i-1)*4+3 ) = 2*dq;
 M( 2*Nq-2+i, (i-1)*4+4 ) = 3*dq*dq;
 M( 2*Nq-2+i, i*4+2) = -1.0;
 rhs(2*Nq-2+i) = 0.0;
end

% N-2 second derivative continuity equations
for i=1:Nq-2
 M( 3*Nq-4+i, (i-1)*4+3 ) = 2.0;
 dq = qmesh(i+1)-qmesh(i);
 M( 3*Nq-4+i, (i-1)*4+4 ) = 6*dq;
 M( 3*Nq-4+i, i*4+3) = -2.0;
 rhs(3*Nq-4+i) = 0.0;
end

% left boundary first derivative to zero
M(4*Nq-6+1, 2) = 1.0;
rhs(4*Nq-6+1) = (vals(2)-vals(1)) / (qmesh(2)-qmesh(1));

% right boundary first derivative to zero
if 1
dq = qmesh(Nq)-qmesh(Nq-1);
M(4*Nq-6+2, (Nq-2)*4+2) = 1.0;
M(4*Nq-6+2, (Nq-2)*4+3) = 2*dq;
M(4*Nq-6+2, (Nq-2)*4+4) = 3*dq*dq;
rhs(4*Nq-6+2) = (vals(Nq)-vals(Nq-1)) / (qmesh(Nq)-qmesh(Nq-1));
end
b = M \ rhs;
x = linspace(1e-5,1.0,1000);
for i=1:prod(size(b))
  fprintf(fid,'%.15g,', b(i));
  if fmod(i,4) == 0
    fprintf(fid, '\n');
  end
end

xx=[];
yy=[];
for i=1:1000
x0 = x(i); % Interpolate x0
i=find(x0 < qmesh);
lower_boundary = qmesh(i(1)-1);
y0 = b( (i(1)-2)*4 + 1 ); % Constant
dq = x0 - lower_boundary;
y0 = y0 + b( (i(1)-2)*4 + 2 ) * dq;
y0 = y0 + b( (i(1)-2)*4 + 3 ) * dq*dq;
y0 = y0 + b( (i(1)-2)*4 + 4 ) * dq*dq*dq;
xx = [xx x0];
yy = [yy y0];
end
%plot(xx,gradient(yy,xx),'k');
plot(xx,yy,'k');
ylim([-0.1 1.1]);
hold on


end
end
