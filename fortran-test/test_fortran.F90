#if defined HAVE_CONFIG_H
#include "config.h"
#endif

module libvdwxc_m
  interface
    subroutine vdwxc_new(functional, vdw)
      integer, intent(in) :: functional
      integer, pointer, intent(out) :: vdw
    end subroutine vdwxc_new
  end interface

  interface
    subroutine vdwxc_print(vdw)
      integer, pointer, intent(in) :: vdw
    end subroutine vdwxc_print
  end interface

  interface
    subroutine vdwxc_set_unit_cell(vdw, nx, ny, nz,&
      C00, C01, C02, C10, C11, C12, C20, C21, C22)
      integer, pointer, intent(inout) :: vdw
      integer, intent(in) :: nx, ny, nz
      real(8), intent(in) :: C00, C01, C02, C10, C11, C12, C20, C21, C22
    end subroutine vdwxc_set_unit_cell
  end interface

  interface
    subroutine vdwxc_init_serial(vdw)
      integer, pointer, intent(inout) :: vdw
    end subroutine vdwxc_init_serial
  end interface

#if defined HAVE_MPI
  interface
    subroutine vdwxc_init_mpi(vdw, mpi_comm)
      integer, pointer, intent(inout) :: vdw
      integer,          intent(in)    :: mpi_comm
    end subroutine vdwxc_init_mpi
  end interface

#if defined HAVE_PFFT
  interface
    subroutine vdwxc_init_pfft(vdw, mpi_comm, nx, ny)
      integer, pointer, intent(inout) :: vdw
      integer,          intent(in)    :: mpi_comm
      integer,          intent(in)    :: nx, ny
    end subroutine vdwxc_init_pfft
  end interface
#endif
#endif

  interface
    subroutine vdwxc_calculate(vdw, rho_g, sigma_g, dedrho_g, dedsigma_g, energy)
      integer, pointer, intent(inout) :: vdw
      ! rho and sigma should probably be intent(in)
      real(8), intent(inout) :: rho_g(:,:,:), sigma_g(:,:,:)
      real(8), intent(inout) :: dedrho_g(:,:,:), dedsigma_g(:,:,:)
      real(8), intent(inout) :: energy
    end subroutine vdwxc_calculate
  end interface

  interface
    subroutine vdwxc_finalize(vdw)
      integer, pointer, intent(inout) :: vdw
    end subroutine vdwxc_finalize
  end interface

end module libvdwxc_m

program test
  use libvdwxc_m
  call vdwxc_hello()

contains

  subroutine vdwxc_hello

    use mpi
    use libvdwxc_m

    implicit none

    integer, pointer :: vdw

    real(8), dimension(9) :: cell
    integer(4) :: nx, ny, nz
    integer :: mpi_err

    real(8) :: energy
    real(8), dimension(2,1,1) :: rho_g, sigma_g, dedrho_g, dedsigma_g

    rho_g(1,1,1) = 3.0
    sigma_g(1,1,1) = 3.0
    rho_g(2,1,1) = 2.980066577841241404
    sigma_g(2,1,1) = 2.980066577841241404

    dedrho_g = 0.0
    dedsigma_g = 0.0

    cell(:) = 0.0
    cell(1) = 8.1
    cell(5) = 9.1
    cell(9) = 10.1

    nx = 2
    ny = 1
    nz = 1

    call mpi_init(mpi_err)

    call vdwxc_new(1, vdw)
    call vdwxc_set_unit_cell(vdw, &
      nz, ny, nx, &
      cell(1), cell(2), cell(3), &
      cell(4), cell(5), cell(6), &
      cell(7), cell(8), cell(9))
    !call vdwxc_init_serial(vdw);
    print*, 'now init mpi'
#if defined HAVE_PFFT
    call vdwxc_init_pfft(vdw, MPI_COMM_WORLD, 1, 1)
#elif defined HAVE_MPI
    call vdwxc_init_mpi(vdw, MPI_COMM_WORLD)
#else
    print*, '*** init mpi skipped ***'
#endif
    print*, 'init mpi done'
    ! XXX figure out how to pass communicator from Fortran to C
    !call vdwxc_init_mpi(vdw, comm);
    call vdwxc_print(vdw)
    call vdwxc_calculate(vdw, rho_g, sigma_g, dedrho_g, dedsigma_g, energy)
    call vdwxc_finalize(vdw)

#if defined HAVE_MPI
    call mpi_finalize(mpi_err)
#endif

    print*, 'energy', energy
  end subroutine vdwxc_hello

end program test
